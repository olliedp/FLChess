﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class AIController : PlayerController {
    
	Piece pieceToMove;
	Tile targetTile;
	int numMovesTried;
	int numTempCaptured;

	public override void PlayerStateUpdated(TurnState newState) {
		switch (newState) {
			case TurnState.Selecting:
				Minimax(4, int.MinValue, int.MaxValue);
				SelectPiece(pieceToMove);
				StartCoroutine(delayAfterSelect());
			break;

			case TurnState.Moving:
				if (targetTile.OccupyingPiece) {
					StartCoroutine(player.gameplayManager.RollForPiece(selectedPiece, targetTile.OccupyingPiece));
					player.TurnActionsRemaining--;
					UnselectPiece();
					player.UpdateState(TurnState.Attacking);
				} else {
					selectedPiece.GetComponent<PieceMovement>().Move(targetTile);
					player.TurnActionsRemaining--;
					UnselectPiece();
					StartCoroutine(delayAfterMove());
				}
			break;

			case TurnState.TurnEnd:
				ReleaseControl();
			break;
		}
	}

	private IEnumerator delayAfterSelect() {
		yield return new WaitForSeconds(0.5f);

		player.UpdateState(TurnState.Moving);
	}

	private IEnumerator delayAfterMove() {
		yield return new WaitForSeconds(0.5f);

		player.UpdateState(TurnState.Selecting);
	}

	private void decideMove() {
		List<Tile> legalMoves;
		while (true) {
			pieceToMove = player.Pieces[Random.Range(0, player.Pieces.Count)];
			legalMoves = pieceToMove.GetComponent<PieceMovement>().GetLegalTiles();

			if (legalMoves.Count > 0) { break; }
		}

		targetTile = legalMoves[Random.Range(0, legalMoves.Count)];
	}

	private void Minimax(int depth, int alpha, int beta) {
		Tile bestMove;
		Piece bestPiece;

		numTempCaptured = 0;
		numMovesTried = 0;
		max(depth-1, out bestPiece, out bestMove, alpha, beta);

		//Debug.Log($"Best move selected is {bestMove} {bestPiece}");
		pieceToMove = bestPiece;
		targetTile = bestMove;

		//Debug.Log($"Num moves tried {numMovesTried} {numTempCaptured}");
	}

	private int max(int depth, out Piece bestPiece, out Tile bestMove, int alpha, int beta) {
        int bestScore = int.MinValue;
		bestMove = null;
		bestPiece = null;

		if (depth == 0) {
			int currentScore = evaluateBoard(false);
			if (currentScore != 0) {
				numMovesTried++;
			}
			return currentScore;
		}

		Player currentPlayer = player;
        bool prune = false;
		foreach (Piece piece in currentPlayer.Pieces) {
            if (prune)
                break;
			PieceMovement pieceMovement = piece.GetComponent<PieceMovement>();
			foreach (Tile tile in pieceMovement.GetLegalTiles()) {
				Tile originalTile = piece.tile;
				Piece capturedPiece = null;
				bool originalHasEverMoved = pieceMovement.hasEverMoved;

				// Try move
				if (tile.OccupyingPiece) {
					capturedPiece = tile.OccupyingPiece;
					capturedPiece.tempCaptured = true;
				}
				pieceMovement.hasEverMoved = true;
				piece.SetTile(tile, false);

				originalTile.chessBoard.cacheUpdateNumber++;
				originalTile.BroadcastUpdate();
				tile.BroadcastUpdate();
				
				//Debug.Log("(White) SEtting has moved of "+piece+" to true");		

				int newScore = min(depth-1, alpha, beta);
				//Debug.Log($"MaxScore {newScore}");

                if (newScore >= bestScore)
                {
                    //Debug.Log($"New max {newScore} {bestScore}");
                    bestMove = tile;
                    bestPiece = piece;
                    bestScore = newScore;
                }

				// Undo move
				pieceMovement.hasEverMoved = originalHasEverMoved;
				piece.SetTile(originalTile, false);
				
				//Debug.Log("(White) SEtting has moved of "+piece+" to original ("+originalHasEverMoved+")");	
				if (capturedPiece != null) {
					capturedPiece.SetTile(tile, false);
					capturedPiece.tempCaptured = false;
				}

				originalTile.chessBoard.cacheUpdateNumber++;
				originalTile.BroadcastUpdate();
				tile.BroadcastUpdate();

                alpha = Mathf.Max(alpha, bestScore);
                if (beta <= alpha)
                {
                    prune = true;
                    break;
                }
            }
		}

		return bestScore;
	}

	private int min(int depth, int alpha, int beta) {
		int bestScore = int.MaxValue;

		if (depth == 0) {
			int currentScore = evaluateBoard(true);
			if (currentScore != 0) {
				numMovesTried++;
			}
			return currentScore;
		}

		Player currentPlayer = player.gameplayManager.players[0];
        bool prune = false;
		foreach (Piece piece in currentPlayer.Pieces) {
            if (prune)
                break;
			PieceMovement pieceMovement = piece.GetComponent<PieceMovement>();
			foreach (Tile tile in pieceMovement.GetLegalTiles()) {
				Tile originalTile = piece.tile;
				Piece capturedPiece = null;
				bool originalHasEverMoved = pieceMovement.hasEverMoved;

				// Try move
				if (tile.OccupyingPiece) {
					capturedPiece = tile.OccupyingPiece;
					capturedPiece.tempCaptured = true;
				}
				pieceMovement.hasEverMoved = true;
				piece.SetTile(tile, false);

				originalTile.chessBoard.cacheUpdateNumber++;
				originalTile.BroadcastUpdate();
				tile.BroadcastUpdate();
				
				//Debug.Log("(Black) SEtting has moved of "+piece+" to true");	

				Tile dummyTile;
				Piece dummyPiece;
				int newScore = max(depth-1, out dummyPiece, out dummyTile, alpha, beta);
				//Debug.Log($"MinScore {newScore}");

				if (newScore <= bestScore) {
					//Debug.Log($"New min {newScore} {bestScore}");
					bestScore = newScore;
				}

				// Undo move
				pieceMovement.hasEverMoved = originalHasEverMoved;
				piece.SetTile(originalTile, false);
				
				//Debug.Log("(Black) SEtting has moved of "+piece+" to original ("+originalHasEverMoved+")");	
				if (capturedPiece != null) {
					capturedPiece.SetTile(tile, false);
					capturedPiece.tempCaptured = false;
				}

				originalTile.chessBoard.cacheUpdateNumber++;
				originalTile.BroadcastUpdate();
				tile.BroadcastUpdate();

                beta = Mathf.Min(newScore, beta);
                if (beta <= alpha)
                {
                    prune = true;
                    break;
                }
            }
		}

		return bestScore;
	}

	private int evaluateBoard(bool danger) {
		float boardScore = 0;

		//numMovesTried++;
		foreach (Player p in player.gameplayManager.players) {
			foreach (Piece piece in p.Pieces) {
				if (!piece.tempCaptured) {
                    boardScore += (float)piece.score;

                } else {
					numTempCaptured++;
				}
			}

            int pIndex = (player == player.gameplayManager.players[0]) ? 0 : 1;
            int eIndex = (pIndex == 0) ? 1 : 0;
            foreach(Piece pPiece in player.gameplayManager.players[pIndex].Pieces)
                foreach(Piece ePiece in player.gameplayManager.players[eIndex].Pieces)
                {
                    float x = Mathf.Abs(pPiece.tile.Position.x - ePiece.tile.Position.x);
                    float y = Mathf.Abs(pPiece.tile.Position.y - ePiece.tile.Position.y);

                    boardScore += (float)(ePiece.score) / 10 + (7 - (x + y));
                }

        }

        return (int)boardScore;
	}
}
