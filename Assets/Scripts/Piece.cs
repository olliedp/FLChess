﻿using UnityEngine;

public class Piece : MonoBehaviour {
	public Player owner;
	public Tile tile;
	public int strength;

	public int score;
	public bool tempCaptured = false;

	public void Awake() {
		 //score = (strength+1) * 10;
	}

	public void SetTile(Tile tile, bool visuallyUpdate=true) {
		Tile oldTile = this.tile;
        if (oldTile) {
            this.tile.OccupyingPiece = null;
		}

        this.tile = tile;
        tile.OccupyingPiece = this;

		if (visuallyUpdate) {
			transform.parent = tile.transform;
			transform.localPosition = new Vector3(0, 0, -0.25f);
		}
    }

	public float AttackSuccessChance(Piece piece) {
        return 1 - AttackMinimumRoll(piece) / 6f;
    }

	public int AttackMinimumRoll(Piece piece) {
		return Mathf.Clamp(4 + (piece.strength - this.strength), 1, 6);
	}
}
