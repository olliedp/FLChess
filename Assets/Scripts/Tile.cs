﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //private GameObject board;


    private bool highlighted = false;
    public bool Highlighted { get { return highlighted; } }

	public ChessBoard chessBoard;
    private GameObject highlighter;
    
	private Sprite[] sprites;
	private GameObject indicatorPrefab;
	private GameObject difficultyIndicator;
	private List<PieceMovement> subscriptions = new List<PieceMovement>();

	[SerializeField]
    private Piece occupyingPiece;
    public Piece OccupyingPiece
    {
        get { return occupyingPiece; }
        set { occupyingPiece = value; }
    }

    [SerializeField]
    private Vector2 position;
	public Vector2 Position { get { return position; } } 

	void Start ()
    {
		indicatorPrefab = Resources.Load("Prefabs/CaptureDifficulty", typeof(GameObject)) as GameObject;
		chessBoard = GameObject.Find("Chess Board").GetComponent<ChessBoard>();

        var children = GetComponentsInChildren<Transform>(true);
        highlighter = children[1].gameObject;
		sprites = Resources.LoadAll<Sprite>("Sprites/BlueDie");
	}

    public void ToggleHighlight()
    {
        highlighted = !highlighted;
        highlighter.SetActive(highlighted);
    }

	public void SetCaptureDifficulty(int difficulty) {
		if (difficulty == 0) {
			if (difficultyIndicator != null) { 
				Destroy(difficultyIndicator);
				difficultyIndicator = null;
			}
		} else {
			if (difficultyIndicator == null) {
				difficultyIndicator = Instantiate(indicatorPrefab);
				difficultyIndicator.transform.parent = transform;
				difficultyIndicator.transform.localScale = new Vector3(0.1f,0.1f,1.0f);
				difficultyIndicator.transform.localPosition = new Vector3(0.16f,-0.5f,0.0f);
			}

			difficultyIndicator.GetComponent<SpriteRenderer>().sprite = sprites[difficulty-1];
		}
	}

	public void BroadcastUpdate() {
		var oldSubscriptions = subscriptions;
		subscriptions = new List<PieceMovement>();
		foreach (PieceMovement pieceMovement in oldSubscriptions) {
			pieceMovement.UpdateLegalTilesCache();
		}
	}

	public void Subscribe(PieceMovement pieceMovement) {
		subscriptions.Add(pieceMovement);
	}
}
