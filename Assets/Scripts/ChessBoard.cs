﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChessBoard : MonoBehaviour {
    public int Rows { get; private set; }
    public int Columns { get; private set; }
	public Tile[,] Tiles { get; private set; }

	public int cacheUpdateNumber = 0;

	public void Start() {
		Rows = transform.childCount;
		Columns = transform.GetChild(0).childCount;

		Tiles = new Tile[Rows, Columns];
		for (int row=0; row < Rows; row++) {
			for (int col=0; col < Columns; col++) {
				Tiles[row,col] = transform.GetChild(row).GetChild(col).GetComponent<Tile>();
			}
		}
	}
}
