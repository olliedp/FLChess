﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerController : MonoBehaviour {
	protected Player player;
	protected Piece selectedPiece;

	protected List<Tile> highlightedTiles = new List<Tile>();

	private void Start () {
		player = GetComponent<Player>();
	}

	public abstract void PlayerStateUpdated(TurnState newState);

	protected void ReleaseControl() {
		UnselectPiece();
	}

	protected void SelectPiece(Piece piece) {
		if (selectedPiece) {
			UnselectPiece();
		}

		selectedPiece = piece;

		var legalMoves = piece.GetComponent<PieceMovement>().GetLegalTiles();
		HighlightTiles(legalMoves);
	}
	
	protected void UnselectPiece() {
		UnhighlightTiles();
		selectedPiece = null;
	}

	private void HighlightTiles(List<Tile> tiles) {
		UnhighlightTiles();

		foreach (Tile tile in tiles) {
			tile.ToggleHighlight();

			if (tile.OccupyingPiece) {
				tile.SetCaptureDifficulty(selectedPiece.AttackMinimumRoll(tile.OccupyingPiece));
			}
		}

		highlightedTiles = tiles;
	}

	private void UnhighlightTiles() {
		if (highlightedTiles != null) {
			foreach (Tile tile in highlightedTiles) {
				tile.SetCaptureDifficulty(0);
				tile.ToggleHighlight();
			}
		}
		
		highlightedTiles = null;
	}
}