﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGenerator : MonoBehaviour
{
    /*GameObject[,] tiles;

    [SerializeField]
    GameObject blackTile;

    [SerializeField]
    GameObject whiteTile;

    public int rows = 8;
    public int columns = 8;

	public void Generate()
    {
        GameObject board = new GameObject("Chess Board");
        board.tag = "Chess Board";
        ChessBoard boardProps = board.AddComponent<ChessBoard>();
        boardProps.SetSize(rows, columns);

        tiles = new GameObject[rows, columns];

        // Used to switch between black and white tiles
        bool tileToggle = true;     

		for (int i = 0; i < rows; i++)
        {
            // Used to keep things more organized in the editor
            GameObject rowObject = new GameObject("Row " + i);
            rowObject.transform.parent = boardProps.transform;

            for (int j = 0; j < columns; j++)
            {
                GameObject tile;

                if (tileToggle)
                    tile = blackTile;
                else
                    tile = whiteTile;

                // Create a tile, move to correct position, and set the row as its parent
                tiles[i, j] = Instantiate(tile, new Vector3(j * 1f, i * 1f, 0), transform.rotation);
                Tile tComponent = tiles[i, j].GetComponent<Tile>();
                tComponent.SetBoard(board);
                tComponent.SetPosition(j, i);

                tiles[i, j].name = "Tile X: " + j + "  Y: " + i;

                tiles[i, j].transform.parent = rowObject.transform;

                tileToggle = !tileToggle;
            }
            if (columns % 2 == 0)
                tileToggle = !tileToggle;
        }

        boardProps.Tiles = tiles;
	}
	
	public GameObject GetTile(int x, int y)
    {
        return tiles[x, y];
    }*/
}
