﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGUI : MonoBehaviour
{
    [SerializeField]
    private GameplayManager gameplayManager;

	// Use this for initialization
	void Start () {
		gameplayManager = GameObject.Find("Gameplay Manager").GetComponent<GameplayManager>();
	}
	

    void OnGUI()
    {
        var player = gameplayManager.activePlayer;

        if (player != null)
        {
			var remainingTime = gameplayManager.GetRemainingTurnTime() + 1;
			var remainingTimeText = remainingTime == 0 ? "Unlimited" : string.Format("{0}:{1:00}", (int)remainingTime / 60, (int)remainingTime % 60);

            GUI.Label(new Rect(10, 10, 150, 50), "Turn: " + player.name);
            GUI.Label(new Rect(10, 60, 150, 50), "Actions left: " + gameplayManager.activePlayer.TurnActionsRemaining);
			GUI.Label(new Rect(10, 80, 150, 50), "Time left: " + remainingTimeText);
        }
    }
}
