﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Since Unity is awful, we have to implement coroutines properly so we can return results
 * 
 * Example usage:
 * // Using IEnumerator
 * ReturningCoroutine<bool> waitForRoll = new ReturningCoroutine<bool>(this, WaitForRoll(attacker, target));
 * yield return waitForRoll.coroutine;
 * bool rollSuccess = waitForRoll.result;
 * 
 * // WaitForRoll(attacker, target)
 * yield return WaitForSeconds(...);
 * yield return false; // final result is returned
 */
public class ReturningCoroutine<T> {
	private IEnumerator _target;
	public T result;
	public Coroutine coroutine { get; private set; }
 
	public ReturningCoroutine(MonoBehaviour owner_, IEnumerator target_) {
		_target = target_;
		coroutine = owner_.StartCoroutine(Run());
	}
 
	private IEnumerator Run() {
		while(_target.MoveNext()) {
			if (_target.Current.GetType() == typeof(T)) {
				result = (T)_target.Current;
				yield return result;
			} else {
				yield return _target.Current;
			}
		}
	}
	
}
