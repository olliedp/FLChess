﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * Broken because Unity is a sack of shit.
 */
public abstract class Singleton {
	static Dictionary<System.Type, object> singletons = new Dictionary<System.Type, object>();

	public static T Get<T>() where T : ScriptableObject {
		System.Type type = typeof(T);

		if (!singletons.ContainsKey(type)) { // Caching
			Debug.Log("Creating new");
			var singleton = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
			Debug.Log($"Gotttt {singleton}");
			singletons.Add(type, singleton);
		}

		Debug.Log($"Result is {singletons[type]}");
		return (T)singletons[type];
	}
}