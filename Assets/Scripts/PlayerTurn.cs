﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurn : MonoBehaviour
{/*
    public GameObject currentPlayer;

    public GameObject selectedPiece;
    public List<Tile> legalMoves;
    public List<Tile> enemiesInRange;

    public bool inPlay = true;

	public float turnBegin = 0;
	public bool paused;

    public ChessBoard chessBoard;

    [SerializeField]
    private GameplayManager gameplayManager;

	private bool fastMode = false;
    private int action = 2;
	private float pausedAt;

	GameObject successMarker;
	GameObject failureMarker;

	// Use this for initialization
	void Start ()
    {
        currentPlayer = gameplayManager.GetCurrentPlayer();
		
		successMarker = Resources.Load("Prefabs/RollSuccess", typeof(GameObject)) as GameObject;
		failureMarker = Resources.Load("Prefabs/RollFail", typeof(GameObject)) as GameObject;
	}

	void ToggleTime() {
		paused = !paused;

		if (paused) {
			pausedAt = Time.time;
		} else {
			turnBegin = turnBegin + (Time.time - pausedAt);
		}
	}

    void Update()
    {
		if (fastMode && GetRemainingTime() <= 0) {
			if (currentState == TurnState.Moving) {
				ToggleHighlightTiles();
			}

			action = 2;
			gameplayManager.NextTurn();

			return;
		}

        // Check to see if a tile was clicked and act accordingly
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

            if (hit.collider != null && hit.collider.tag == "Tile")
            {
                Tile t = hit.collider.GetComponent<Tile>();

                switch (currentState)
                {
					case TurnState.Attacking:
						//Do nothing. Rolling in background
						break;
                    case TurnState.Selecting:
                        // If the tile has a piece on it, select that piece
                        if (t.OccupyingPiece != null && GetPlayer(t) == currentPlayer)
                        {
                            SelectPiece(t.OccupyingPiece);
                            UpdateState(TurnState.Moving);
                        }
                        break;

                    case TurnState.Moving:
						
                        // If the tile is highlighted (legal move), move piece to that tile
                        if (t.Highlighted)
                        {
							ToggleHighlightTiles();
							action--;

                            if (t.OccupyingPiece == null) {
                                selectedPiece.GetComponent<PieceMovement>().Move(t);

								if (action > 0) {
									UpdateState(TurnState.Selecting);
								} else {
									action = 2;
									gameplayManager.NextTurn();
								}
							} else {
								var coroutine = RollForPiece(selectedPiece, t.GetComponent<Tile>().occupyingPiece);

								UpdateState(TurnState.Attacking);
								StartCoroutine(coroutine);
							}
                        }

                        else if (GetPlayer(t) == currentPlayer)
                        {
                            ToggleHighlightTiles();

                            SelectPiece(t.OccupyingPiece);
                            UpdateState(TurnState.Moving);
                        }
                        break;
                }
            }
        }
    }
	
	public void UpdateState(TurnState newState)
    {
        currentState = newState;
		switch(currentState)
        {
            case TurnState.Moving:
                ToggleHighlightTiles();
                break;

            case TurnState.None:
                var pieces = currentPlayer.GetComponent<PlayerPieces>();
                for (int i = 0; i < pieces.Size; i++)
                {
                    var movement = pieces.GetPiece(i).GetComponent<PieceMovement>();
                    movement.ResetMoves();
                }
                break;
        }
	}

    public void SelectPiece(GameObject piece)
    {
        selectedPiece = piece;
        legalMoves = selectedPiece.GetComponent<PieceMovement>().GetLegalMoves();
        enemiesInRange = selectedPiece.GetComponent<PieceMovement>().GetEnemiesInRange();
        string debug = "Enemy pieces found at tiles: ";
        foreach (Tile t in enemiesInRange)
        {
            debug += t.GetPosition() + ", ";
        }
        Debug.Log(debug);
    }

    public int GetRemainingActions()
    {
        return action;
    }

	public float  GetRemainingTime() {
		return fastMode ? 2*60 - (Time.time - turnBegin) + (paused ? Time.time - pausedAt : 0)  : -1;
	}

    private GameObject GetPlayer(Tile t)
    {
        return t.OccupyingPiece.GetComponent<PieceProperties>().player;
    }

    private void ToggleHighlightTiles()
    {
		Debug.Log($"Toggling highlights {legalMoves.Count}, {enemiesInRange.Count}");
        for (int i = 0; i < legalMoves.Count; i++) {
            legalMoves[i].ToggleHighlight();
		}
        for (int i = 0; i < enemiesInRange.Count; i++) {
            enemiesInRange[i].ToggleHighlight();
			if (enemiesInRange[i].Highlighted) {
				int difficulty = (int) Mathf.RoundToInt((1 - selectedPiece.GetComponent<PieceMovement>().AttackSuccessChance(enemiesInRange[i].GetComponent<Tile>().occupyingPiece)) * 6);
				float test = ((1 - selectedPiece.GetComponent<PieceMovement>().AttackSuccessChance(enemiesInRange[i].GetComponent<Tile>().occupyingPiece)) * 6);
				Debug.Log($"Difficulty {difficulty} {test}");

				enemiesInRange[i].SetCaptureDifficulty(difficulty);
			} else {
				enemiesInRange[i].SetCaptureDifficulty(0);
			}
		}
			
    }

	private IEnumerator RollForPiece(GameObject attacker, GameObject target) {
		ToggleTime();

		yield return gameObject.GetComponent<DieRoller>().RollDie();
		float rolledChance = GetComponent<DieRoller>().lastRollResult;
		float requiredChance  = attacker.GetComponent<PieceMovement>().AttackSuccessChance(target);

		Camera currentCamera = Camera.main;
		GameObject die = GetComponent<DieRoller>().lastDie;
		Vector3 diePosition = die.transform.position;
		Vector3 cameraSpace = currentCamera.WorldToScreenPoint(diePosition);
		cameraSpace -= new Vector3(0,0,cameraSpace.z);
		Vector3 markerPosition = currentCamera.ScreenToWorldPoint(cameraSpace);
		markerPosition -= new Vector3(0,0,markerPosition.z);

		GameObject marker = Instantiate(rolledChance <= requiredChance ? successMarker : failureMarker);
		marker.transform.position = markerPosition + new Vector3(0,0,marker.transform.position.z);
		marker.transform.parent = transform.parent;

		for (int i=0; i <= 4; i++) { 
			yield return new WaitForSeconds(0.25f);
			marker.GetComponent<SpriteRenderer>().enabled = i%2 != 0;
		}

		yield return new WaitForSeconds(0.25f);
		Destroy(die);
		Destroy(marker);

		if (rolledChance <= requiredChance) {
			Debug.Log($"Capture successful {rolledChance}, {requiredChance}");
			attacker.GetComponent<PieceMovement>().Capture(target);
		} else {
			Debug.Log($"Capture failure {rolledChance}, {requiredChance}");
		}
		
		if (action > 0) {
			UpdateState(TurnState.Selecting);
		} else {
			action = 2;
			gameplayManager.NextTurn();
		}

		ToggleTime();
	}*/
}
