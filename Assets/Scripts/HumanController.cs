﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : PlayerController {
	public override void PlayerStateUpdated(TurnState newState) {
		switch (newState) {
			case TurnState.TurnEnd:
				ReleaseControl();
			break;
		}
	}

	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			var clickedTile = GetTargetTile();

			if (clickedTile) {
				TileClicked(clickedTile);
			}
		}
	}

	private Tile GetTargetTile() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (hit.collider != null && hit.collider.tag == "Tile") {
            return hit.collider.GetComponent<Tile>();
		}

		return null;
	}

	private void TileClicked(Tile tile) {
		var targetPiece = tile.OccupyingPiece;

		switch (player.currentState) {
			case TurnState.Selecting:
				StateSelectingClick(tile, targetPiece);
			break;

			case TurnState.Moving:
				StateMovingClick(tile, targetPiece);
			break;
		}
	}

	private void StateSelectingClick(Tile tile, Piece piece) {
		if (piece && piece.owner == player) {
			SelectPiece(piece);
			player.UpdateState(TurnState.Moving);
		}
	}

	private void StateMovingClick(Tile tile, Piece piece) {
		if (piece) {
			if (piece.owner == player) {
				SelectPiece(piece);
				player.UpdateState(TurnState.Moving);

				return;
			} else if (highlightedTiles.Contains(tile)) {
				StartCoroutine(player.gameplayManager.RollForPiece(selectedPiece, piece));
				player.TurnActionsRemaining--;
				UnselectPiece();
				player.UpdateState(TurnState.Attacking);

				return;
			}
		} else {
			if (highlightedTiles.Contains(tile)) {
				selectedPiece.GetComponent<PieceMovement>().Move(tile);
				player.TurnActionsRemaining--;
				UnselectPiece();
				player.UpdateState(TurnState.Selecting);
				

				return;
			}
		}

		// If click target was not valid, unselect the piece
		UnselectPiece();
		player.UpdateState(TurnState.Selecting);
	}
}
