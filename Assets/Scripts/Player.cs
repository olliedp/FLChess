﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/*
 * Each turn involves 3 states
 * 
 * TurnStart: Set up data for turn
 * Selecting: choosing which piece to move
 * Moving: deciding where to move the selected piece
 * Attacking: Waiting for attack roll to complete
 * TurnEnd: Time to cleanup anything, if need be
 * 
 */
public enum TurnState {
    TurnBegin, Selecting, Moving, Attacking, TurnEnd
}

public class Player : MonoBehaviour {
    [SerializeField]
	private List<Piece> pieces;
	public ReadOnlyCollection<Piece> Pieces {
		get { return pieces.AsReadOnly(); }
	}

	public int TurnActionsRemaining { get; set; }

	public TurnState currentState = TurnState.TurnEnd;
	
	public GameplayManager gameplayManager;

	public void Start() {
		gameplayManager = GameObject.Find("Gameplay Manager").GetComponent<GameplayManager>();
	}

	public void UpdateState(TurnState newState) {
		currentState = newState;

		switch(currentState) {
			case TurnState.TurnBegin:
				TurnActionsRemaining = 2;
				UpdateState(TurnState.Selecting);
			break;

            case TurnState.Selecting:
				Debug.Log($"{this} Entered select state with {TurnActionsRemaining} actions remaining");
				if (TurnActionsRemaining == 0) {
					gameplayManager.NextTurn();
					return;
				}
            break;

            case TurnState.TurnEnd:
				foreach (Piece piece in pieces) {
					piece.GetComponent<PieceMovement>().ResetState();
				}
            break;
        }

		GetComponent<PlayerController>().PlayerStateUpdated(newState);
	}

	public void PieceCaptured(Piece piece) {
		pieces.Remove(piece);
	}
}
