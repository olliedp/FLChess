﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class MoveGenerator : MonoBehaviour
{/*
    public static ChessBoard chessBoard;

    private static Dictionary<PieceType, float> pieceValues;

    void Awake()
    {
        chessBoard = GameObject.FindWithTag("Chess Board").GetComponent<ChessBoard>();

        pieceValues = new Dictionary<PieceType, float>();

        pieceValues.Add(PieceType.King, 900);
        pieceValues.Add(PieceType.Queen, 90);
        pieceValues.Add(PieceType.Bishop, 50);
        pieceValues.Add(PieceType.Rook, 50);
        pieceValues.Add(PieceType.Knight, 20);
        pieceValues.Add(PieceType.Pawn, 10);
    }

	// Use this for initialization
	void Start ()
    {
        
	}
	
	public static float GetPieceValue(PieceType type)
    {
        return pieceValues[type];
    }

    

    public static List<Piece> GetEnemiesInRange(Piece piece, List<Piece> otherPieces)
    {
        var pieces = new List<Piece>();

        foreach (Piece p in otherPieces)
        {
            if (p.properties.pieceColor != piece.properties.pieceColor)
            {
                int diffX = Mathf.Abs(p.x - piece.x);
                int diffY = Mathf.Abs(p.y - piece.y);

                if (diffX <= 1 && diffY <= 1)
                    pieces.Add(p);
            }
        }

        return pieces;
    }


    public static float AttackSuccessChance(Piece atk, Piece def)
    {
        int enemyStrength = def.properties.strength;

        int minAttack = Mathf.Clamp(4 + (enemyStrength - atk.properties.strength), 1, 6);

        Debug.Log($"Min attack {minAttack}");
        return 1 - minAttack / 6f;
    }*/
}
