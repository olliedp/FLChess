﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameSettings : ScriptableObject {
	[Header("Players")]

	[SerializeField]
	private int actionsPerTurn;
	public int ActionsPerTurn { get; }
}

/*public static class GameSettingsLoader2 {
	public static GameSettings globalSettings;

	[RuntimeInitializeOnLoadMethod]
	public static void LoadSettings () {
		Debug.Log("This is running!!!!!");
		if (!globalSettings) {
			//globalSettings = (GameSettings)ScriptableObject.CreateInstance("GameSettings");
			globalSettings = Resources.FindObjectsOfTypeAll<GameSettings>().FirstOrDefault();
		}
	}
}*/