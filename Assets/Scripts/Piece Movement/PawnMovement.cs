﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnMovement : PieceMovement {
    public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		int xMin = Mathf.Max(xCurrent-1, 0);
		int xMax = Mathf.Min(xCurrent+1, chessBoard.Columns-1);
		int yMin = Mathf.Max(yCurrent-1, 0);
		int yMax = Mathf.Min(yCurrent+1, chessBoard.Rows-1);

		bool canAttack = attacksThisTurn == 0 || canAttackTwice;
		for (int x=xMin; x <= xMax; x++) {
			for (int y=yMin; y <= yMax; y++) {
				if (!(x == xCurrent && (y == yCurrent || y == yCurrent + (flipDirection ? -1 : 1)))) { // Current tile not valid
					Tile tile = chessBoard.Tiles[y, x];
					tile.Subscribe(this);

					if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
						legalMoves.Add(tile);
					}
				}
			}
		}

		if (flipDirection ? (yCurrent > yMin) : (yCurrent < yMax)) {
			Tile forwardTile = chessBoard.Tiles[yCurrent + (flipDirection ? -1 : 1), xCurrent];
			forwardTile.Subscribe(this);

			if ((movesThisTurn == 0 && forwardTile.OccupyingPiece == null) || ((attacksThisTurn == 0 || canAttackTwice) && forwardTile.OccupyingPiece && forwardTile.OccupyingPiece.owner != piece.owner)) {
				legalMoves.Add(forwardTile);
			}

			if (!hasEverMoved) {
				Tile initiativeTile = chessBoard.Tiles[yCurrent + (flipDirection ? -2 : 2), xCurrent];
				initiativeTile.Subscribe(this);
				if (forwardTile.OccupyingPiece == null && (initiativeTile.OccupyingPiece == null || initiativeTile.OccupyingPiece.owner != piece.owner)) {
					legalMoves.Add(initiativeTile);
				}
			}
		}

        legalTilesCached = legalMoves;
    }
}
