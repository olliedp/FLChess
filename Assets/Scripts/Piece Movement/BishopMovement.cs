﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BishopMovement : PieceMovement {
	public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		var movesUpLeft = GetOpenTiles(new Vector2(-1,1), 4);
		var movesUpRight = GetOpenTiles(new Vector2(1,1), 4);
		var movesDownLeft = GetOpenTiles(new Vector2(-1,-1), 4);
		var movesDownRight = GetOpenTiles(new Vector2(1,-1), 4);

		foreach (var tile in movesUpLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesUpRight) { legalMoves.Add(tile); }
		foreach (var tile in movesDownLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesDownRight) { legalMoves.Add(tile); }

		bool canAttack = attacksThisTurn == 0 || canAttackTwice;
		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		if (yCurrent > 0) {
			Tile tile = chessBoard.Tiles[yCurrent-1, xCurrent];
			tile.Subscribe(this);
			if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
				legalMoves.Add(tile);
			}
		}

		if (yCurrent+1 < chessBoard.Rows) {
			Tile tile = chessBoard.Tiles[yCurrent+1, xCurrent];
			tile.Subscribe(this);
			if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
				legalMoves.Add(tile);
			}
		}

		if (xCurrent > 0) {
			Tile tile = chessBoard.Tiles[yCurrent, xCurrent-1];
			tile.Subscribe(this);
			if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
				legalMoves.Add(tile);
			}
		}

		if (xCurrent+1 < chessBoard.Rows) {
			Tile tile = chessBoard.Tiles[yCurrent, xCurrent+1];
			tile.Subscribe(this);
			if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
				legalMoves.Add(tile);
			}
		}

		legalTilesCached = legalMoves;
	}
}
