﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

using SysRandom = System.Random;

//[RequireComponent(typeof(PieceProperties))]
public abstract class PieceMovement : MonoBehaviour {
	public bool flipDirection = false;
    public bool canAttackTwice = false;
	
	protected int movesThisTurn = 0;
	protected int attacksThisTurn = 0;
	public bool hasEverMoved { get; set; } = false;
	private int lastCacheUpdate = 0;

	protected Piece piece;
	protected List<Tile> legalTilesCached;

	public void Awake() {
		piece = GetComponent<Piece>();
	}

	// Reset moves/attacks after a turn
	public void ResetState() {
		movesThisTurn = 0;
		attacksThisTurn = 0;
	}

	public virtual void Move(Tile tile) {
		Tile originalTile = piece.tile;

		piece.SetTile(tile);
		hasEverMoved = true;
		movesThisTurn++;

		originalTile.chessBoard.cacheUpdateNumber++;
		originalTile.BroadcastUpdate();
		tile.BroadcastUpdate();
	}

	public void Capture(Piece piece) {
		Tile originalTile = this.piece.tile;

		// Get rid of the captured piece
        piece.owner.PieceCaptured(piece);
		Destroy(piece.gameObject);

		// Move this piece to the captured piece's old tile
        this.piece.SetTile(piece.tile);
		attacksThisTurn++;

		originalTile.chessBoard.cacheUpdateNumber++;
		originalTile.BroadcastUpdate();
		piece.tile.BroadcastUpdate();
	}

	public void FailCapture(Piece piece) {
		attacksThisTurn++;
	}

	public void UpdateLegalTilesCache() {
		if (lastCacheUpdate != piece.tile.chessBoard.cacheUpdateNumber) {
			lastCacheUpdate = piece.tile.chessBoard.cacheUpdateNumber;

			UpdateLegalTiles();
		}
	}

	public List<Tile> GetLegalTiles() {
		if (legalTilesCached == null) {
			UpdateLegalTiles();
		}

		return legalTilesCached;
	}

    public abstract void UpdateLegalTiles();

	protected List<Tile> GetOpenTiles(Vector2 direction, int length) {
		List<Tile> openTiles = new List<Tile>();

		bool canAttack = attacksThisTurn == 0 || canAttackTwice;
		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		Vector2 target = location + direction*length;
		int xDirection = (int)direction.x, yDirection = (int)direction.y;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		if (xDirection != 0) {
			int xFinal = (int)Mathf.Clamp(target.x, 0, chessBoard.Columns-1);
			int xDelta = Mathf.Abs(xFinal-xCurrent);
			length = Mathf.Min(xDelta, length);
		}

		if (yDirection != 0) {
			int yFinal = (int)Mathf.Clamp(target.y, 0, chessBoard.Rows-1);
			int yDelta = Mathf.Abs(yFinal-yCurrent);
			length = Mathf.Min(yDelta, length);
		}

		bool skip = false;
		for (int i=0; i < length; i++) {
			Tile nextTile = chessBoard.Tiles[yCurrent+yDirection*(i+1), xCurrent+xDirection*(i+1)];
			nextTile.Subscribe(this);

			if (!skip) {
				if (nextTile.OccupyingPiece) {
					if (canAttack && nextTile.OccupyingPiece.owner != piece.owner) {
						openTiles.Add(nextTile);
					}

					skip = true;
				} else {
					openTiles.Add(nextTile);
				}
			}
            
		}

		return openTiles;
	}
}
