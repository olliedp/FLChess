﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(PieceProperties))]
public class KingMovement : PieceMovement {
	public void Start() {
	}

	public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		int xMin = Mathf.Max(xCurrent-1, 0);
		int xMax = Mathf.Min(xCurrent+1, chessBoard.Columns-1);
		int yMin = Mathf.Max(yCurrent-1, 0);
		int yMax = Mathf.Min(yCurrent+1, chessBoard.Rows-1);

		for (int x=xMin; x <= xMax; x++) {
			for (int y=yMin; y <= yMax; y++) {
				if (!(x == xCurrent && y == yCurrent)) { // Current tile not valid
					Tile tile = chessBoard.Tiles[y, x];
					tile.Subscribe(this);

					if (tile.OccupyingPiece == null || ((attacksThisTurn == 0 || canAttackTwice) && tile.OccupyingPiece.owner != piece.owner)) {
						legalMoves.Add(tile);
					}
				}
			}
		}

		// Can only perform castle if king has never moved and we have room left for moving both the king and rook
		if (!hasEverMoved) {
			Tile tileRookRight = chessBoard.Tiles[yCurrent, xCurrent + 3];
			Tile tileRookLeft = chessBoard.Tiles[yCurrent, xCurrent -4];
			Tile tileRight1 = chessBoard.Tiles[yCurrent, xCurrent + 1];
			Tile tileRight2 = chessBoard.Tiles[yCurrent, xCurrent + 2];
			Tile tileLeft1 = chessBoard.Tiles[yCurrent, xCurrent - 1];
			Tile tileLeft2 = chessBoard.Tiles[yCurrent, xCurrent - 2];
			Piece rookRight = tileRookRight.OccupyingPiece;
			Piece rookLeft = tileRookLeft.OccupyingPiece;

			tileRookLeft.Subscribe(this);
			tileRookRight.Subscribe(this);
			tileRight1.Subscribe(this);
			tileLeft1.Subscribe(this);
			tileRight2.Subscribe(this);
			tileLeft2.Subscribe(this);

			if (rookRight) {
				if (!rookRight.GetComponent<PieceMovement>().hasEverMoved && tileRight1.OccupyingPiece == null && tileRight2.OccupyingPiece == null) {
					legalMoves.Add(tileRight2);
                }
			}
			if (rookLeft) {
				if (!rookLeft.GetComponent<PieceMovement>().hasEverMoved && tileLeft1.OccupyingPiece == null && tileLeft2.OccupyingPiece == null) {
					legalMoves.Add(tileLeft2);
                }
			}
		}

		legalTilesCached = legalMoves;
	}

	public override void Move(Tile tile) {
		var horizontalDistanceMoved = tile.Position.x - piece.tile.Position.x;
		bool isCastle = Mathf.Abs(horizontalDistanceMoved) == 2;

		if (isCastle) {
			ChessBoard chessBoard = piece.tile.chessBoard;

			Vector2 location = piece.tile.Position;
			Piece rook = chessBoard.Tiles[(int)location.y, (int)location.x + (horizontalDistanceMoved < 0 ? -4 : 3)].OccupyingPiece;
			Tile skippedTile = chessBoard.Tiles[(int)location.y, (int)location.x + (int)horizontalDistanceMoved/2];
			Tile originalRookTile = rook.tile;

			piece.owner.TurnActionsRemaining--; // This move counts as two, so decrement in addition to default
			rook.GetComponent<PieceMovement>().Move(skippedTile);

			tile.chessBoard.cacheUpdateNumber++;
			originalRookTile.BroadcastUpdate();
			skippedTile.BroadcastUpdate();
		}

		base.Move(tile);
	}
}
