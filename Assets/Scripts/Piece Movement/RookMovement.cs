﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RookMovement : PieceMovement {
	public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		var movesUp = GetOpenTiles(new Vector2(0,1), 4);
		var movesDown = GetOpenTiles(new Vector2(0,-1), 4);
		var movesLeft = GetOpenTiles(new Vector2(-1,0), 4);
		var movesRight = GetOpenTiles(new Vector2(1,0), 4);

		foreach (var tile in movesUp) { legalMoves.Add(tile); }
		foreach (var tile in movesDown) { legalMoves.Add(tile); }
		foreach (var tile in movesLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesRight) { legalMoves.Add(tile); }

		bool canAttack = attacksThisTurn == 0 || canAttackTwice;
		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		if (xCurrent > 0) {
			if (yCurrent > 0) {
				Tile tile = chessBoard.Tiles[yCurrent-1, xCurrent-1];
				tile.Subscribe(this);
				if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
					legalMoves.Add(tile);
				}
			}

			if (yCurrent+1 < chessBoard.Rows) {
				Tile tile = chessBoard.Tiles[yCurrent+1, xCurrent-1];
				tile.Subscribe(this);
				if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
					legalMoves.Add(tile);
				}
			}
		}

		if (xCurrent+1 < chessBoard.Columns) {
			if (yCurrent > 0) {
				Tile tile = chessBoard.Tiles[yCurrent-1, xCurrent+1];
				tile.Subscribe(this);
				if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
					legalMoves.Add(tile);
				}
			}

			if (yCurrent+1 < chessBoard.Rows) {
				Tile tile = chessBoard.Tiles[yCurrent+1, xCurrent+1];
				tile.Subscribe(this);
				if (canAttack && tile.OccupyingPiece && tile.OccupyingPiece.owner != piece.owner) {
					legalMoves.Add(tile);
				}
			}
		}

		legalTilesCached = legalMoves;
	}
}
