﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenMovement : PieceMovement {
	public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		var movesUp = GetOpenTiles(new Vector2(0,1), 4);
		var movesDown = GetOpenTiles(new Vector2(0,-1), 4);
		var movesLeft = GetOpenTiles(new Vector2(-1,0), 4);
		var movesRight = GetOpenTiles(new Vector2(1,0), 4);
		var movesUpLeft = GetOpenTiles(new Vector2(-1,1), 4);
		var movesUpRight = GetOpenTiles(new Vector2(1,1), 4);
		var movesDownLeft = GetOpenTiles(new Vector2(-1,-1), 4);
		var movesDownRight = GetOpenTiles(new Vector2(1,-1), 4);

		foreach (var tile in movesUp) { legalMoves.Add(tile); }
		foreach (var tile in movesDown) { legalMoves.Add(tile); }
		foreach (var tile in movesLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesRight) { legalMoves.Add(tile); }
		foreach (var tile in movesUpLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesUpRight) { legalMoves.Add(tile); }
		foreach (var tile in movesDownLeft) { legalMoves.Add(tile); }
		foreach (var tile in movesDownRight) { legalMoves.Add(tile); }
		
		legalTilesCached = legalMoves;
	}
}
