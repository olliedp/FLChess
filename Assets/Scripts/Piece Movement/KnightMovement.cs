﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(PieceProperties))]
public class KnightMovement : PieceMovement {
	public override void UpdateLegalTiles() {
		List<Tile> legalMoves = new List<Tile>();

		ChessBoard chessBoard = piece.tile.chessBoard;
		Vector2 location = piece.tile.Position;
		int xCurrent = (int)location.x, yCurrent = (int)location.y;

		int xMin = Mathf.Max(xCurrent-2, 0);
		int xMax = Mathf.Min(xCurrent+2, chessBoard.Columns-1);
		int yMin = Mathf.Max(yCurrent-2, 0);
		int yMax = Mathf.Min(yCurrent+2, chessBoard.Rows-1);

		for (int y=yMin; y <= yMax; y++) {
			if (y != yCurrent) {
				var xDelta = 2/Mathf.Abs(y-yCurrent);
				var xRight = xCurrent+xDelta;
				var xLeft = xCurrent-xDelta;

				if (xRight <= xMax) {
					Tile tile = chessBoard.Tiles[y, xRight];
					tile.Subscribe(this);
					if (tile.OccupyingPiece == null || tile.OccupyingPiece.owner != piece.owner) {
						legalMoves.Add(tile);
					}
				}

				if (xLeft >= xMin) {
					Tile tile = chessBoard.Tiles[y, xLeft];
					tile.Subscribe(this);
					if (tile.OccupyingPiece == null || tile.OccupyingPiece.owner != piece.owner) {
						legalMoves.Add(tile);
					}
				}

			}
		}

		xMin = Mathf.Max(xCurrent-1, 0);
		xMax = Mathf.Min(xCurrent+1, chessBoard.Columns-1);
		yMin = Mathf.Max(yCurrent-1, 0);
		yMax = Mathf.Min(yCurrent+1, chessBoard.Rows-1);

		for (int x=xMin; x <= xMax; x++) {
			for (int y=yMin; y <= yMax; y++) {
				if (!(x == xCurrent && y == yCurrent)) { // Current tile not valid
					Tile tile = chessBoard.Tiles[y, x];
					tile.Subscribe(this);

					if (tile.OccupyingPiece != null && ((attacksThisTurn == 0 || canAttackTwice) && tile.OccupyingPiece.owner != piece.owner)) {
						legalMoves.Add(tile);
					}
				}
			}
		}

        legalTilesCached = legalMoves;
    }
}
