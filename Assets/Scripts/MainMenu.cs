﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; //Allows us to change scenes in Unity. 
	
public class MainMenu : MonoBehaviour
{

    /*
	*Goes to the chess game scene. 
	*/
    public Button playButton;

    void Awake()
    {
        playButton.onClick.AddListener(PlayGame);
    }
    

	public void PlayGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}
