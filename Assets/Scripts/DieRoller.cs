﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieRoller : MonoBehaviour {
	GameObject diePrefab;
	
	public GameObject lastDie;
	public float lastRollResult; //Because Unity is extrmely inept and you can't return values between coroutines

	private float animationStart = -1.0f;

	float ANIMATION_DURATION = 0.25f;
	Quaternion start;
	Quaternion finish;

	// Use this for initialization
	void Start () {
		diePrefab = Resources.Load("Prefabs/Die", typeof(GameObject)) as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
		if (animationStart != -1.0f) { 
			var delta = Time.time - animationStart;
			var alpha = delta / ANIMATION_DURATION;

			//lastDie.transform.rotation = Quaternion.Lerp(start, finish, alpha);

			if (alpha >= 1.0f) {
				animationStart = -1.0f;
			}
		}
	}

	public IEnumerator RollDie() {
		var die = Instantiate(diePrefab);
		die.transform.rotation = Random.rotation;
		die.GetComponent<Rigidbody>().AddTorque(Random.insideUnitSphere*160);
		die.transform.parent = transform.parent;
		die.GetComponent<Rigidbody>().velocity = new Vector3(0,-1,0);

		// Wait for die to settle
		yield return new WaitUntil(() => !die.GetComponent<Die>().rolling);
		Debug.Log($"Settled {die.GetComponent<Die>().value}");

		// Freeze die if there is still some minor velocity
		die.GetComponent<Rigidbody>().detectCollisions = false;
		die.GetComponent<Rigidbody>().useGravity = false;
		die.GetComponent<BoxCollider>().enabled = false;

		// Return roll data
		float rollResult = 1f - (float) die.GetComponent<Die>().value / die.GetComponent<Die>().numSides;
		List<object> rollData = new List<object>() { die, rollResult };
		yield return rollData;
		
		// TODO animation to make settled face point to camera for easier visibility
		/*
		Vector3 hitVector = die.GetComponent<Die>().HitVector(die.GetComponent<Die>().value);
		start = die.transform.rotation;
		
		finish = Quaternion.LookRotation(hitVector, Vector3.up);
		animationStart = Time.time;

		yield return new WaitForSeconds(ANIMATION_DURATION);
		
		yield return null;*/
	}
}
