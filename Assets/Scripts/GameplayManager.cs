﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour 
{
	public ChessBoard chessBoard;

	public List<Player> players; 
	private int activePlayerIndex = 0;
	public Player activePlayer { get { return players[activePlayerIndex]; } }

    public int turnCounter = 0;
	public float turnStart;
	public float pauseStart;
	public bool paused = false;
	public bool fastMode = false;
	public int turnTime = 2 * 60;

	[SerializeField]
	private GUIStyle nextTurnLabelStyle;
	private Player winningPlayer;
	private GameObject successMarker;
	private GameObject failureMarker;

    void Awake() {
		nextTurnLabelStyle.normal.textColor = Color.white;
		nextTurnLabelStyle.alignment = TextAnchor.UpperCenter;
		nextTurnLabelStyle.fontSize = 24;

		successMarker = Resources.Load("Prefabs/RollSuccess", typeof(GameObject)) as GameObject;
		failureMarker = Resources.Load("Prefabs/RollFail", typeof(GameObject)) as GameObject;

		turnStart = Time.time;
		activePlayer.UpdateState(TurnState.TurnBegin);
    }

	void Update() {
		float remainingTurnTime = GetRemainingTurnTime();
		if (fastMode && remainingTurnTime < 0 && !paused) {
			NextTurn();
		}
	}

	private void OnGUI() {
		var currentTime = Time.time;
		var elapsed = currentTime - turnStart;

		if (elapsed < 2) {
			var color = nextTurnLabelStyle.normal.textColor;
			
			color.a = Mathf.Min(1, 2 - elapsed);
			nextTurnLabelStyle.normal.textColor = color;
			nextTurnLabelStyle.fontSize = 24;
			
			GUI.Label(new Rect(Screen.width/2, Screen.height-75, 100, 100), "Turn: " + activePlayer.name, nextTurnLabelStyle);
			
		}

		if (winningPlayer) {
			GUI.Label(new Rect(Screen.width/2, Screen.height/2, 100, 100), $"{winningPlayer.name} wins!");
		}
	}

	public void PlayerWon(Player player) {
		winningPlayer = player;
		StartCoroutine(GoToMenu());
	}

	public IEnumerator GoToMenu() {
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene("test");
	}

	/*
	 * Cycles activePlayer to the next player in the players list(circular)
	 * Returns old and new player as out params
	 */
	private void CyclePlayers(out Player oldPlayer, out Player newPlayer) {
		oldPlayer = activePlayer;
		activePlayerIndex = (activePlayerIndex + 1) % players.Count;
		newPlayer = activePlayer;
	}

	public void NextTurn() {
		PauseTimer(true);
		StartCoroutine(NextPlayerTurnDelay());
    }

	private void PauseTimer(bool isPaused) {
		paused = isPaused;

		if (paused) {
			pauseStart = Time.time;
		} else {
			turnStart = turnStart + (Time.time - pauseStart);
		}
	}

	public float GetRemainingTurnTime() {
		return fastMode ? turnTime - (Time.time - turnStart) + (paused ? Time.time - pauseStart : 0)  : -1;
	}

	private IEnumerator NextPlayerTurnDelay() {
		yield return new WaitForSeconds(0.25f);

		turnStart = Time.time;
		PauseTimer(false);

		Player oldPlayer, newPlayer;
		CyclePlayers(out oldPlayer, out newPlayer);

		oldPlayer.UpdateState(TurnState.TurnEnd);
		newPlayer.UpdateState(TurnState.TurnBegin);
	}

	public IEnumerator RollForPiece(Piece attacker, Piece target) {
		PauseTimer(true);

		// Roll die
		ReturningCoroutine<bool> waitForRoll = new ReturningCoroutine<bool>(this, WaitForRoll(attacker, target));
		yield return waitForRoll.coroutine;

		// If roll was successful, capture the target piece
		bool rollSuccess = waitForRoll.result;
		if (rollSuccess) {
			attacker.GetComponent<PieceMovement>().Capture(target);

			// If we're capturing the king, the game is over
			if (target.GetComponent<KingMovement>()) {
				PlayerWon(attacker.owner);
			}
		} else {
			attacker.GetComponent<PieceMovement>().FailCapture(target);
		}
		
		// Return player to selecting state
		attacker.owner.UpdateState(TurnState.Selecting);

		PauseTimer(false);
	}

	// Roll the die, display the success/fail marker, and return whether the roll was a success
	private IEnumerator WaitForRoll(Piece attacker, Piece target) {
		// Roll die and check if value is high enough to capture target
		ReturningCoroutine<List<object>> dieRoll = new ReturningCoroutine<List<object>>(this, GetComponent<DieRoller>().RollDie());
		yield return dieRoll.coroutine;
		List<object> rollData = dieRoll.result; // Because Unity is dumb and doesn't support tuples

		GameObject die = (GameObject)rollData[0];
		float rolledChance = (float)rollData[1];
		float requiredChance  = attacker.AttackSuccessChance(target);
		bool captureSuccess = rolledChance <= requiredChance;

		// Create/display a marker and make it flash on/off for a few seconds
		GameObject marker = DisplayRollOutcomeMarker(die, captureSuccess);
		marker.transform.parent = transform.parent;
		for (int i=0; i <= 4; i++) { 
			yield return new WaitForSeconds(0.25f);
			marker.GetComponent<SpriteRenderer>().enabled = i%2 != 0;
		}

		// Clean up die/marker so they're not on the board permanently
		yield return new WaitForSeconds(0.25f);
		Destroy(die);
		Destroy(marker);

		// Return whether capture was successful or not
		yield return captureSuccess;
	}

	// Create (not display) a O or X marker depending on whether the roll was a success
	private GameObject DisplayRollOutcomeMarker(GameObject die, bool success) {
		// Find the center of the die in 2D space, as this is where we want to display the marker
		Camera currentCamera = Camera.main;
		Vector3 diePosition = die.transform.position;
		Vector3 cameraSpace = currentCamera.WorldToScreenPoint(diePosition);
		cameraSpace -= new Vector3(0,0,cameraSpace.z);
		Vector3 markerPosition = currentCamera.ScreenToWorldPoint(cameraSpace);
		markerPosition -= new Vector3(0,0,markerPosition.z);

		// Instantiate and display the outcome marker, and move to location calculated above
		GameObject marker = Instantiate(success ? successMarker : failureMarker);
		marker.transform.position = markerPosition + new Vector3(0,0,marker.transform.position.z);

		return marker;
	}
}
