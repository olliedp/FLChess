﻿using UnityEngine;
using System.Collections;
// Attach this to a gameobject that exists in the initial scene
public class GameSettingsLoader : MonoBehaviour {
    /*[Tooltip(“Choose which GameSettings asset to use”)]
    public GameSettings _settings; // drag GameSettings asset here in inspector*/

	// Proxy because Unity doesn't show static variables in editor
	[SerializeField]
	public GameSettings settings;
    public static GameSettings GlobalSettings; 

	static void SceneLoader() {

	}

    void Awake(){
        DontDestroyOnLoad(gameObject); // Don't unload when we're changing scenes; global settings

        if(GlobalSettings==null){
            GlobalSettings = settings;
        }
    }
}